bigquery_ethereum_transactions_counter
---

START:
* pip install -r requirements.txt
* generate JSON-key GoogleCloudPlatform as param 'ACCOUNT_KEY'

IMPLEMENTED:
(examples in '/out' directory)
* query_runner_top.py 
  * Transactions amount by token per day (amount.png)
  * Average transaction volume per day (volume.png)
* query_runner.py
  * Average number of transactions by week/month (amount_by_week.csv, amount_by_month.csv)
  * Average number of coins transacted by week/month (volume_by_week.csv, volume_by_month.csv)
