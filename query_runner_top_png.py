import pandas as pd

from config import (ACCOUNT_KEY, TOKENS_ADDRESS, TOKENS_NAME, ETHER,
                    PNG_AMOUNT, PNG_VOLUME, plt, log)


def run_query_amount_transactions(contract):
    """
    Get transactions data for token from BQ, count amount transactions.
    :param contract: (str) token address | contract address
    """
    return pd.read_gbq(
        'SELECT block_timestamp, COUNT(1) AS tx_count '
        'FROM `bigquery-public-data.ethereum_blockchain.token_transfers` '
        f'WHERE token_address = "{contract}" '
        'GROUP BY block_timestamp ',
        private_key=ACCOUNT_KEY,
        dialect='standard')


def run_query_volume_transactions(contract):
    """
    Get transactions data for token from BQ, count transactions value.
    :param contract: (str) token address | contract address
    """
    return pd.read_gbq(
        'SELECT block_timestamp, CAST(value as FLOAT64) AS value '
        'FROM `bigquery-public-data.ethereum_blockchain.token_transfers` '
        f'WHERE token_address = "{contract}" '
        'GROUP BY block_timestamp, value',
        private_key=ACCOUNT_KEY,
        dialect='standard')


def get_data(query, aggr_column, aggr_func):
    """
    Get and aggregate transactions data, convert Wei to Ether.
    :param query: (func) query in raw SQL for get df from BQ
    :param aggr_column: (str) column for aggregation
    :param aggr_func: (str) function of aggregation
    """
    df_bq = pd.DataFrame()
    i = 0
    for contract in TOKENS_ADDRESS:
        try:
            df = query(contract=contract)
            df['block_timestamp'] = df['block_timestamp'].dt.date
            df = df.groupby('block_timestamp')[aggr_column].agg(aggr_func)
            df_bq = pd.concat([df_bq, df],
                              axis=1,
                              sort=False)
            df_bq.rename({aggr_column: contract},
                         axis=1,
                         inplace=True)
            i += 1
            print(f'{i}. Contract: {contract}')
        except Exception as e:
            log.error(f'Error {e} | Contract {contract}')
            pass
    df_bq = df_bq.groupby(df_bq.index)[TOKENS_ADDRESS].sum()
    df_bq = df_bq.applymap(lambda x: x / ETHER)
    df_bq.rename(columns=dict(zip(TOKENS_ADDRESS, TOKENS_NAME)),
                 inplace=True)
    return df_bq


def get_and_save_graph(df, title, ylabel, path_to_save, scientific):
    """
    Config and save graph figure in .PNG file.
    :param df: (dataframe) from BQ
    :param title: (str) title of graph
    :param ylabel: (str) label of y-axis
    :param path_to_save: (str) path to save graph
    :param scientific: (bool) turn (of | on) scientific mode of y-axis
    """
    plot = df.plot(kind='bar',
                   figsize=(125, 25),
                   title=title)
    plot.set_xlabel('Date')
    plot.set_ylabel(ylabel)
    plot.get_yaxis().get_major_formatter().set_scientific(scientific)
    fig = plot.get_figure()
    fig.subplots_adjust(bottom=0.2)
    plt.savefig(path_to_save)


if __name__ == '__main__':
    get_and_save_graph(df=get_data(run_query_amount_transactions,
                                   'tx_count', 'count'),
                       title='Transactions amount by token',
                       ylabel='Amount',
                       path_to_save=PNG_AMOUNT,
                       scientific=True)

    get_and_save_graph(df=get_data(run_query_volume_transactions,
                                   'value', 'sum'),
                       ylabel='Value (in Ether)',
                       title='Transaction volume in Ether',
                       path_to_save=PNG_VOLUME,
                       scientific=False)
