import pandas as pd
from config import (ACCOUNT_KEY, TOKENS_ADDRESS, TOKENS_NAME,
                    CSV_AMOUNT_BY_WEEK, CSV_AMOUNT_BY_MONTH,
                    CSV_VOLUME_BY_WEEK, CSV_VOLUME_BY_MONTH, log)


def run_query_amount_transactions(contract):
    """
    Get transactions data for token from BQ,
    count amount transactions.
    :param contract: (str) token address or contract address
    """
    return pd.read_gbq(
        'SELECT block_timestamp, COUNT(1) AS tx_count '
        'FROM `bigquery-public-data.ethereum_blockchain.token_transfers` '
        'WHERE token_address = "{0}" '
        'GROUP BY block_timestamp '.format(contract),
        private_key=ACCOUNT_KEY,
        dialect='standard')


def run_query_volume_transactions(contract):
    """
    Get transactions data for token from BQ,
    count transactions value by current token.
    :param contract: (str) token address or contract address
    """
    return pd.read_gbq(
        'SELECT block_timestamp, CAST(value as FLOAT64) AS value '
        'FROM `bigquery-public-data.ethereum_blockchain.token_transfers` '
        f'WHERE token_address = "{contract}" '
        'GROUP BY block_timestamp, value ',
        private_key=ACCOUNT_KEY,
        dialect='standard')


def collect_data(query, aggr_column, aggr_func, csv_by_week, csv_by_month):
    """
    Get and aggregate transactions data, grouping by month & week,
    collect into .CSV files.
    :param query: (func) query in raw SQL for get df from BQ
    :param aggr_column: (str) column for aggregation
    :param aggr_func: (str) function of aggregation
    :param csv_by_week: (str) path to save grouped by week data into .CSV
    :param csv_by_month: (str) path to save grouped by month data into .CSV
    """
    df_bq = pd.DataFrame()
    i = 0
    for contract in TOKENS_ADDRESS:
        try:
            df = query(contract=contract)
            df['block_timestamp'] = df['block_timestamp'].dt.date
            df = df.groupby('block_timestamp')[aggr_column].agg(aggr_func)
            df_bq = pd.concat([df_bq, df],
                              axis=1,
                              sort=False)
            df_bq.rename({aggr_column: contract},
                         axis=1,
                         inplace=True)
            i += 1
            print(f'{i}. Contract: {contract}')
        except Exception as e:
            log.error(f'Error {e} | Contract {contract}')
            pass
    df_bq.index = pd.to_datetime(df_bq.index)
    df_bq.rename(columns=dict(zip(TOKENS_ADDRESS, TOKENS_NAME)),
                 inplace=True)

    df_by_week = df_bq.copy(deep=True)
    df_by_week.index = df_by_week.index.to_period(freq='W-MON')
    df_by_week = df_by_week.groupby(df_by_week.index)[TOKENS_NAME].sum()
    df_by_week = df_by_week.applymap(lambda x: '{:d}'.format(int(x)))
    df_by_week.astype(str).T.to_csv(csv_by_week)

    df_by_month = df_bq.copy(deep=True)
    df_by_month.index = df_by_month.index.to_period(freq='M')
    df_by_month = df_by_month.groupby(df_by_month.index)[TOKENS_NAME].sum()
    df_by_month = df_by_month.applymap(lambda x: '{:d}'.format(int(x)))
    df_by_month.astype(str).T.to_csv(csv_by_month)


if __name__ == '__main__':
    collect_data(query=run_query_amount_transactions,
                 aggr_column='tx_count',
                 aggr_func='count',
                 csv_by_week=CSV_AMOUNT_BY_WEEK,
                 csv_by_month=CSV_AMOUNT_BY_MONTH)
    collect_data(query=run_query_volume_transactions,
                 aggr_column='value',
                 aggr_func='sum',
                 csv_by_week=CSV_VOLUME_BY_WEEK,
                 csv_by_month=CSV_VOLUME_BY_MONTH)
